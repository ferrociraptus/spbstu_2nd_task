#include "FractalCurve.h"
#include <stdlib.h>
#include <math.h>

#define TRIGGER_INIT 1

static Point __find_radius_vector(Point* start, Point* end){
    Point value;
    value.x = end->x - start->x;
    value.y = end->y - start->y;
    return value;
}

static Point __rotate_radius_vector(Point* vector, double angle){
    Point result;
    result.x = vector->x * cos(angle) - vector->y * sin(angle);
    result.y = vector->x * sin(angle) + vector->y * cos(angle);
    return result;
}

static void __decrement_fractal_curve_iteration(FractalCurve* curve){
    switch (curve->type){
        case KOH:
            for (int i = 0; i < curve->list->len - 1; i++){
                remove_point_from_point_list(curve->list, i + 1);
                remove_point_from_point_list(curve->list, i + 1);
                remove_point_from_point_list(curve->list, i + 1);
            }
        break;
        
        case DRAGON:
        case LEVI:
            for (int i = 0; i < curve->list->len - 1; i++)
                remove_point_from_point_list(curve->list, i + 1);
        break;
        
        case SERP_TRIANGLE:
            curve->trigger *= -1;
            for (int i = 0; i < curve->list->len - 1; i++){
                remove_point_from_point_list(curve->list, i + 1);
                remove_point_from_point_list(curve->list, i + 1);
            }
        break;
    }
}

static void __increment_fractal_curve_iteration(FractalCurve* curve){
    switch (curve->type){
        case KOH:
            for (int i = 0; i < curve->list->len - 1; i += 4){
                // sp  p2  p3  ep
                // *---*---*---*
                Point start_point = *get_point_from_point_list(curve->list, i);
                Point end_point = *get_point_from_point_list(curve->list, i+1);
                Point radius_vector = __find_radius_vector(&start_point, &end_point);
                multiply_point(&radius_vector, 1.0/3);
                Point* point2 = point_summ(&start_point, &radius_vector);
                Point* point3 = point_summ(point2, &radius_vector);
                
                radius_vector = __rotate_radius_vector(&radius_vector, -M_PI/3);
                
                Point* apex = point_summ(point2, &radius_vector);
                insert_point_to_point_list(curve->list, i + 1, point3);
                insert_point_to_point_list(curve->list, i + 1, apex);
                insert_point_to_point_list(curve->list, i + 1, point2);
            }
        break;
        
        case DRAGON:
        case LEVI:
            for (int i = 0; i < curve->list->len - 1; i += 2){
                Point start_point = *get_point_from_point_list(curve->list, i);
                Point end_point = *get_point_from_point_list(curve->list, i+1);
                
                Point radius_vector = __find_radius_vector(&start_point, &end_point);
                radius_vector = __rotate_radius_vector(&radius_vector, curve->trigger * M_PI_4);
                multiply_point(&radius_vector, ((double)1)/sqrt(2));
                
                Point* apex = point_summ(&radius_vector, &start_point);
                insert_point_to_point_list(curve->list, i + 1, apex);
                
                if (curve->type == DRAGON)
                    curve->trigger *= -1;
            }
        break;
        
        case SERP_TRIANGLE:
            for (int i = 0; i < curve->list->len - 1; i += 3){
                Point start_point = *get_point_from_point_list(curve->list, i);
                Point end_point = *get_point_from_point_list(curve->list, i+1);
                
                Point half_of_radius_vec = __find_radius_vector(&start_point, &end_point);
                multiply_point(&half_of_radius_vec, 0.5);
                
                Point rotated_radius_vec;
                rotated_radius_vec = __rotate_radius_vector(&half_of_radius_vec, curve->trigger * M_PI/3);
                
                Point* insert_point1 = point_summ(&rotated_radius_vec, &start_point);
                Point* insert_point2 = point_summ(insert_point1, &half_of_radius_vec);
                
                insert_point_to_point_list(curve->list, i + 1, insert_point1);
                insert_point_to_point_list(curve->list, i + 2, insert_point2);
                
                curve->trigger *= -1;
            }
        break;
    }
}

FractalCurve* new_fractal_curve(FractalCurveType init_type){
    FractalCurve* curve = (FractalCurve*)malloc(sizeof(FractalCurve));
    if (curve == NULL)
        return NULL;
    
    curve->list = new_point_list(2);
    set_point_in_point_list(curve->list, 0, new_point(0, 0));
    set_point_in_point_list(curve->list, 1, new_point(0, 0));
    curve->iteration = 0;
    curve->trigger = TRIGGER_INIT;
    curve->type = init_type;
    
    if (curve->list == NULL){
        free(curve);
        return NULL;
    }
    return curve;
}

void destroy_fractal_curve(FractalCurve* curve){
    if (curve != NULL){
        destroy_point_list(curve->list);
        free(curve);
    }
}

int change_fractal_curve_iteration(FractalCurve* curve, int new_N){
    int diff = curve->iteration - new_N;
    
    if (curve->list->len < 2)
        return -1;
    
    for (int iter = 0; iter < abs(diff); iter++){
        if (diff < 0)
            __increment_fractal_curve_iteration(curve);
        else
            __decrement_fractal_curve_iteration(curve);
    }
    curve->iteration = new_N;
    
    if (new_N == 0)
        reset_fractal_curve_trigger(curve);
    
    return 1;
}

void change_fractal_curve_base(FractalCurve* curve, Point* point1, Point* point2){
    clear_point_list(curve->list);
    append_point_to_point_list(curve->list, point1);
    append_point_to_point_list(curve->list, point2);
    update_fractal_curve(curve);
    
}

void update_fractal_curve(FractalCurve* curve){
    int iteration = curve->iteration;
    curve->iteration = 0;
    reset_fractal_curve_trigger(curve);
    change_fractal_curve_iteration(curve, iteration);
}

void reset_fractal_curve_trigger(FractalCurve* curve) {
    if (curve->type == SERP_TRIANGLE)
        curve->trigger = -TRIGGER_INIT;
    else
        curve->trigger = TRIGGER_INIT;
        
}

void change_fractal_curve_type(FractalCurve* curve, FractalCurveType new_type){
    curve->type = new_type;
    Point* point1 = copy_point(get_point_from_point_list(curve->list, 0));
    Point* point2 = copy_point(get_point_from_point_list(curve->list, 1));
    change_fractal_curve_base(curve, point1, point2);
}
