#ifndef FRACTAL_CURVE_H
#define FRACTAL_CURVE_H

#include "PointList.h"

typedef enum {KOH, DRAGON, LEVI, SERP_TRIANGLE} FractalCurveType;

typedef struct {
    int trigger;
    unsigned int iteration;
    FractalCurveType type;
    PointList* list;
} FractalCurve;

FractalCurve* new_fractal_curve(FractalCurveType init_type);
void destroy_fractal_curve(FractalCurve* curve);
void change_fractal_curve_base(FractalCurve* curve, Point* point1, Point* point2);
int change_fractal_curve_iteration(FractalCurve* curve, int new_iter);
void update_fractal_curve(FractalCurve* curve);
void reset_fractal_curve_trigger(FractalCurve* curve);
void change_fractal_curve_type(FractalCurve* curve, FractalCurveType new_type);

#endif
