WARNINGS = -Wall
DEBUG = -ggdb -fno-omit-frame-pointer
OPTIMIZE = -O0
CC = gcc

all: spbstu_programming_task_2

spbstu_programming_task_2: spbstu_programming_task_2.c fractal_curve.o
	$(CC) `pkg-config --cflags --libs gtk+-3.0`-lm -o $@ $(WARNINGS) $(DEBUG) $(OPTIMIZE) spbstu_programming_task_2.c *.o
	
point.o: Point.c Point.h
	$(CC) $(WARNINGS) $(DEBUG) $(OPTIMIZE) -c -o $@ Point.c
	
point_list_node.o: PointListNode.c PointListNode.h point.o
	$(CC) $(WARNINGS) $(DEBUG) $(OPTIMIZE) -c -o $@ PointListNode.c
	
point_list.o: PointList.c PointList.h point_list_node.o point.o
	$(CC) $(WARNINGS) $(DEBUG) $(OPTIMIZE) -c -o $@ PointList.c
	
fractal_curve.o: FractalCurve.c FractalCurve.h point_list.o
	$(CC) $(WARNINGS) $(DEBUG) $(OPTIMIZE) -c -o $@ FractalCurve.c
	
clean:
	rm -f spbstu_programming_task_2
	rm *.o

install:
	echo "Installing is not supported"

run:
	./spbstu_programming_task_2
