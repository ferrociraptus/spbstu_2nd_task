#ifndef MY_POINT_H
#define MY_POINT_H

typedef struct _Point{
    double x;
    double y;
} Point;

Point* new_point(double x, double y);
void destroy_point(Point *point);
Point* copy_point(Point *point);
void copy_point_val(Point *from, Point *to);
Point* point_summ(Point *point1, Point *point2);
Point* subtract_point_to_point(Point *point, Point *point2);

void multiply_point(Point* point, double value);
void add_to_point_value(Point *point, double x, double y);
void add_to_point_point(Point *point, Point *point2);

char is_point_equal(Point *point, Point *point2);

#endif
