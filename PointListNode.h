#ifndef POINT_LIST_NODE_H
#define POINT_LIST_NODE_H

#include "Point.h"

typedef struct point_list_node{
    struct point_list_node* next_node;
    struct point_list_node* previous_node;
    Point* point;
} PointListNode;

PointListNode* new_point_list_node(Point* point);
void destroy_point_list_node(PointListNode *node);

#endif
